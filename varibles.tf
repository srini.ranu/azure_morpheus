variable "parent_group_id" {
  type    = string
  default = ""
  
}
variable "subscription_id" {
  type    = list(string)
  default = ["484de8b7-695b-4073-8058-dff14463e521"]
  description = "Please provide subscription id's to attach to management group"
}
#variable "ARM_CLIENT_ID" {
#  type    = list(string)
#  default = ["67aae1e0-18d3-4f6a-b277-71b72be514091"]
 ## description = "Please provide subscription id's to attach to management group"
#}
#variable "ARM_CLIENT_SECRET" {
#  default = ["xZ.7Q~CHOfIe0G1AsV57vqbsi5.LYqERLmwgO"]
 ## description = "Please provide subscription id's to attach to management group"
#}

#variable "workload" {
#  type    = string
#  default = ""
#  description = "Please provide workload as Dev, Test etc"
#}
#
#variable "parent_group_name" {
#  type    = string
#  default = ""
#  description = "Please provide name for parent management group"
#}

variable "child_group_name" {
  type    = string
  default = ""
  description = "Please provide name for child management group"
}

variable "policy_name" {
  type    = string
  default = ""
  description = "Please provide policy name to attach it to parent management group"
}

variable "rg_name" {
  type    = string
  default = ""
  description = "Please provide name for resource group"
}
variable "environment" {
  type    = string
  default = ""
 }

variable "location" {
  type    = string
  default = ""
  description = "Please provide location in which you want to create resource group"
}

variable "security_group_name" {
  type    = string
  default = ""
}

variable "vnet_name" {
  type    = string
  default = ""
}
variable "subnet_name" {
  type    = string
  default = ""
}
variable "vnet_cidr" {
  type    = list(string)
  default = ["10.0.0.0/16"]
}

variable "subnet_cidr" {
  type    = list(string)
   default = ["10.0.0.0/25"]
}
